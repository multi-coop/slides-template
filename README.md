# Slides Template

# :warning: This project is now archived. :warning:

**This template is deprecated since it has been improved and replaced by [`slider-template`](https://gitlab.com/multi-coop/slider-template).**

---

This is a template for setting up a slides presentation on GitLab Pages, written in Markdown and using [`reveal-md`](https://github.com/webpro/reveal-md) / [`reveal.js`](http://lab.hakim.se/reveal-js).

Adapted to GitLab Pages by @johanricher from [martinmurphy's project on GitHub](https://github.com/martinmurphy/slidestemplate).

# Get started

*  [Fork the repo](https://gitlab.com/jailbreak/slides-template/-/forks/new),
*  Edit the content in the [`/slides`](./slides) directory,
*  [View the presentation](https://jailbreak.gitlab.io/slides-template/slides.html) (`{your-namespace}.gitlab.io/slides-template/`).

# Customize

You can configure your presentation's theme [in the YAML front matter of your Markdown file](./slides/slides.md#L5) by using one of the available [built-in themes](https://github.com/hakimel/reveal.js/tree/master/css/theme/source):

- beige
- black
- blood
- league
- moon
- night
- serif
- simple
- sky
- solarized
- white

By default, this template uses a custom theme: [`simple-custom.css`](./theme/simple-custom.css).

# Run on your computer

Install `reveal-md`:
```
npm install
```
then to serve and open the presentation in a browser:
```
npm run presentation
```
or to generate a static website in the `public` subdirectory:
```
npm run generate
```
