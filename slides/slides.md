---
title: Slides Template
separator: <!--s-->
verticalSeparator: <!--v-->
theme: theme/simple-custom.css
revealOptions:
    transition: 'fade'
---

# Getting started

*  [Fork the repo](https://gitlab.com/jailbreak/slides-template/-/forks/new),
*  Edit the content in the `/slides` directory,
*  View your presentation on `{your-namespace}.gitlab.io/slides-template/`

<!--v-->

# On your computer

Install `reveal-md`:
```
npm install
```
then to serve and open the presentation in a browser:
```
npm run presentation
```
or to generate a static website in the `public` subdirectory:
```
npm run generate
```

<!--s-->

# References

* [reveal-md](https://github.com/webpro/reveal-md)
* [reveal.js](http://lab.hakim.se/reveal-js)
* [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
* [GitLab CI](https://docs.gitlab.com/ee/ci/)
* [This project](https://gitlab.com/jailbreak/slides-template/)
* [The original project](https://github.com/martinmurphy/slidestemplate) 🙏